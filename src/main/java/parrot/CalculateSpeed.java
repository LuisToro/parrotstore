package parrot;

public interface CalculateSpeed {
	double calculateSpeed();
}
